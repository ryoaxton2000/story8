$(document).ready(() => {
    $.ajax({
        method: 'GET',
        url : 'https://www.googleapis.com/books/v1/volumes?q=random',
        success: function(response) {
            $('tbody').empty();
            for(let i=0; i < response.items.length; i++) {
                var row = document.createElement('tr');
                $(row).append('<th scope="row">' + (i+1) + '</th>');
                if(response.items[i].volumeInfo.title == undefined) {
                    $(row).append('<td class="judul">No Title</td>');
                }
                else {
                $(row).append('<td class="judul">' + response.items[i].volumeInfo.title + '</td>');
                }
                if(response.items[i].volumeInfo.description == undefined) {
                    $(row).append('<td class="deskripsi">No description available</td>');
                }
                else {
                    $(row).append('<td class="deskripsi">' + response.items[i].volumeInfo.description + '</td>');
                }
                if(response.items[i].volumeInfo.authors == undefined) {
                    $(row).append('<td class="link">Author Unknown</td>');
                }
                else {
                $(row).append('<td class="link">' + response.items[i].volumeInfo.authors + '</td>');
                }
                if(response.items[i].volumeInfo.imageLinks.thumbnail == undefined) {
                    $(row).append('<td class="gambar">No image available</td>');
                }
                else {
                    $(row).append('<td class="gambar"><img src="' + response.items[i].volumeInfo.imageLinks.thumbnail + '"></td>');
                }
                $('tbody').append(row);
            }
        }
    })


    $('#button').click(function() {
        let key  = $('#search').val();
        $.ajax({
            method: 'GET',
            url : 'https://www.googleapis.com/books/v1/volumes?q=' + key,
            success: function(response) {
                $('tbody').empty();
                for(let i=0; i < response.items.length; i++) {
                    var row = document.createElement('tr');
                    $(row).append('<th scope="row">' + (i+1) + '</th>');
                    if(response.items[i].volumeInfo.title == undefined) {
                        $(row).append('<td class="judul">No Title</td>');
                    }
                    else {
                    $(row).append('<td class="judul">' + response.items[i].volumeInfo.title + '</td>');
                    }
                    if(response.items[i].volumeInfo.description == undefined) {
                        $(row).append('<td class="deskripsi">No description available</td>');
                    }
                    else {
                        $(row).append('<td class="deskripsi">' + response.items[i].volumeInfo.description + '</td>');
                    }
                    if(response.items[i].volumeInfo.authors == undefined) {
                        $(row).append('<td class="link">Author Unknown</td>');
                    }
                    else {
                    $(row).append('<td class="link">' + response.items[i].volumeInfo.authors + '</td>');
                    }
                    if(response.items[i].volumeInfo.imageLinks.thumbnail == undefined) {
                        $(row).append('<td class="gambar">No image available</td>');
                    }
                    else {
                        $(row).append('<td class="gambar"><img src="' + response.items[i].volumeInfo.imageLinks.thumbnail + '"></td>');
                    }
                    $('tbody').append(row);
                }
            }
        })
    })
})